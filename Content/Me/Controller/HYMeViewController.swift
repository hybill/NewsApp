//
//  HYMeViewController.swift
//  NewsApp
//
//  Created by YWQ on 2/13/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit

class HYMeViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.green
    }
}
