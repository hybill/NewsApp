//
//  TitleModel.swift
//  NewsApp
//
//  Created by YWQ on 2/13/18.
//  Copyright © 2018 YWQ. All rights reserved.
//
/*
{
    "tip_new" : 0,
    "name" : "热点",
    "web_url" : "",
    "default_add" : 1,
    "icon_url" : "",
    "type" : 4,
    "category" : "news_hot",
    "concern_id" : "",
    "flags" : 0
}
*/
import Foundation
import HandyJSON
struct HomeNewsTitle:HandyJSON {
    var tip_new:Int = 0
    var name: String = ""
    var web_url:String = ""
    var default_add:Int = 0
    var icon_url:String = ""
    var type:Int = 0
    var category:String = ""
    var concern_id:String = ""
    var flags:Int = 0
    var selected:Bool = true
}
