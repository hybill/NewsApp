//
//  HYNewsViewController.swift
//  NewsApp
//
//  Created by YWQ on 2/13/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit
import SGPagingView
import RxCocoa
import UIColor_Hex_Swift

class HYNewsViewController: UIViewController {
    
    //标题和内容
    private var pageTitleView:SGPageTitleView?
    private var pageContenView:SGPageContentView?
    let titles = ["11111","22222","33333","44444","55555","66666","77777"]
    
    //自定义导航栏
    private lazy var navigationBar = HYhomeNavigationView()
//    private lazy var disposeBag = dispos
//    private var TitleView:SGpa
    //添加频道按钮
    private lazy var addChannelButton:UIButton = {
        let addChannelButton = UIButton(frame: CGRect(x: screenWidth - titleHeight, y: 0, width: titleHeight, height: titleHeight))
        addChannelButton.backgroundColor = UIColor.white
        if let image = UIImage(named: "添加.png"){
            addChannelButton.setImage(image, for: UIControlState.normal)
        }
        let separatorView = UIView(frame:CGRect(x: 0, y: titleHeight-1, width: titleHeight, height: 1))
        separatorView.backgroundColor = UIColor.white
        addChannelButton.addSubview(separatorView)
        return addChannelButton
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //设置状态栏属性
        navigationController?.navigationBar.barStyle = .default
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.backgroundColor = UIColor("#bc403b")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        clickAction()
    }
}

// MARK 导航栏按钮点击

extension HYNewsViewController{
    private func setupUI(){
        self.view.backgroundColor = UIColor.white
        //设置自定义导航栏
        self.navigationItem.titleView = navigationBar
        //添加频道
        view.addSubview(addChannelButton)
        //title配置
        NetworkTool.loadHomeNewsTitleData {
        let configuration = SGPageTitleViewConfigure()
        configuration.titleColor = UIColor.black
//        configuration.titleSelectedColor
        configuration.indicatorColor = UIColor.clear
        //标题名称数组
        self.pageTitleView = SGPageTitleView(frame: CGRect(x: 0, y: 0, width: screenWidth - titleHeight, height: titleHeight), delegate: self, titleNames: $0.flatMap({ $0.name }), configure: configuration)
        self.pageTitleView?.backgroundColor = UIColor.clear
        self.view.addSubview(self.pageTitleView!)
        //随机默认视图
        _ = $0.flatMap({ (newsTitle) -> () in
            print(newsTitle)
            let vc = UIViewController()
            vc.view.backgroundColor = UIColor.randomColor
            self.addChildViewController(vc)
        })
            //内容视图
            self.pageContenView = SGPageContentView(frame: CGRect(x: 0, y: titleHeight, width: screenWidth, height: self.view.frame.height - titleHeight), parentVC: self, childVCs: self.childViewControllers)
            self.pageContenView?.delegatePageContentView = self
            self.view.addSubview(self.pageContenView!)
        
          
        }
        
    }
    private func clickAction(){
        //搜索按钮点击
        navigationBar.didSelectSearchButton = {
            
        }
        //头像点击按钮
        navigationBar.didSelectAvatarButton = { [weak self] in
            self!.navigationController?.pushViewController(HYMeViewController(), animated: true)
            
        }
        //相机按钮点击
        navigationBar.didSelectCameraButton = {
            
        }
        //频道点击按钮
//        addChannelButton.rx.controlEvent(UIControlEvents.touchUpInside).subscribe(onNext:{
//            [weak self] in
//            let home
//        })
    }
}

//MARK: - SGPageTitleViewDelegate
extension HYNewsViewController:SGPageTitleViewDelegate,SGPageContentViewDelegate{
    //联动 pageContent
    func pageTitleView(_ pageTitleView: SGPageTitleView!, selectedIndex: Int) {
        self.pageContenView?.setPageContentViewCurrentIndex(selectedIndex)
    }
    
    //联动 SGPageTitleView
    func pageContentView(_ pageContentView: SGPageContentView!, progress: CGFloat, originalIndex: Int, targetIndex: Int) {
        self.pageTitleView?.setPageTitleViewWithProgress(progress, originalIndex: originalIndex, targetIndex: targetIndex)
    }
}
