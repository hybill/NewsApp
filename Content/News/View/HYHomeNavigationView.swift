//
//  HYHomeNavigationView.swift
//  NewsApp
//
//  Created by YWQ on 2/14/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import IBAnimatable
import UIKit
import SnapKit

class HYhomeNavigationView: UIView {
    var searchButton:UIButton!
    var avatarButton:UIButton!
    var cameraButton:UIButton!
    //搜索按钮
    var didSelectSearchButton:(() ->())?
    // 头像按钮点击
    var didSelectAvatarButton: (()->())?
    // 相机按钮点击
    var didSelectCameraButton: (()->())?
    override var frame: CGRect{
        didSet{
            super.frame = CGRect(x: -10, y: 0, width: screenWidth, height: 40)
        }
    }

//     固有的大小
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor("#bc403b")
        SetButton()
    
    }
    override func layoutSubviews() {
        cameraButton?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.height.width.equalTo(40)
            make.right.equalTo(-10)
        })
        avatarButton?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.height.width.equalTo(40)
            make.left.equalTo(10)
        })
        searchButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.right.equalTo(cameraButton.snp.left).offset(-10)
            make.left.equalTo(avatarButton.snp.right).offset(10)
        }
    }
    func SetButton(){
        searchButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        avatarButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        cameraButton = UIButton(frame:CGRect(x: 0, y: 0, width: 40, height: 40))
        searchButton.backgroundColor = UIColor.white
        avatarButton.backgroundColor = UIColor.clear
        cameraButton.backgroundColor = UIColor.clear
        if let image = UIImage(named: "头像.png"){
            avatarButton.setImage(image, for: UIControlState.normal)
        }else{
            print("图片未找到")
        }
        if let image = UIImage(named: "照相.png"){
            cameraButton.setImage(image, for: UIControlState.normal)
        }
        searchButton.addTarget(self, action: #selector(searchButtonCliked(sender:)), for: UIControlEvents.touchUpInside)
        avatarButton.addTarget(self, action: #selector(avatarButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        cameraButton.addTarget(self, action: #selector(cameraButtonClicked(sender:)), for: UIControlEvents.touchUpInside)
        self.addSubview(searchButton!)
        self.addSubview(avatarButton!)
        self.addSubview(cameraButton!)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //搜索
    @objc func searchButtonCliked(sender:UIButton){
        print("搜索")
    }
    //头像
    @objc func avatarButtonClicked(sender:UIButton){
        print("头像")
    }
    //相机
    @objc func cameraButtonClicked(sender:UIButton){
        print("相机")
    }
}

