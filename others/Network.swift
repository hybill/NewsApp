//
//  Network.swift
//  NewsApp
//
//  Created by YWQ on 2/16/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol NetworkToolProtocol {
    
    //新闻标题数据
    static func loadHomeNewsTitleData(completionHandler:@escaping (_ newsTitles:[HomeNewsTitle])->())
}

extension NetworkToolProtocol{
    static func loadHomeNewsTitleData(completionHandler:@escaping (_ newsTitles:[HomeNewsTitle])->()){
        let url = BASE_URL + "/article/category/get_subscribed/v1/?"
        let params = ["device_id": device_id,"iid": iid]
        Alamofire.request(url, parameters: params).responseJSON { (response) in
            switch response.result{
            case .success( _):
                if let value = response.result.value {
                    let json = JSON(value)
                    print("get json")
                    guard json["message"] == "success" else {return}
                    if let dataDict = json["data"].dictionary{
                        if let datas = dataDict["data"]?.arrayObject{
                            var titles = [HomeNewsTitle]()
                            titles.append(HomeNewsTitle.deserialize(from: "{\"category\": \"\", \"name\": \"推荐\"}")!)
                            titles += datas.flatMap({ HomeNewsTitle.deserialize(from: $0 as? Dictionary) })
                            completionHandler(titles)
                        }
                    }
                }
            case .failure(let Error):
                print("error")
                print(Error)
            }
        }
    }
}
struct NetworkTool:NetworkToolProtocol {}
