//
//  extensionUIColor.swift
//  NewsApp
//
//  Created by YWQ on 2/15/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    open class var randomColor:UIColor{
        get
        {
            let red = CGFloat(arc4random()%256)/255.0
            let green = CGFloat(arc4random()%256)/255.0
            let blue = CGFloat(arc4random()%256)/255.0
            return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        }
    }
}
