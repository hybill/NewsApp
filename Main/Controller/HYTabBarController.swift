//
//  HYTabBarController.swift
//  NewsApp
//
//  Created by YWQ on 2/13/18.
//  Copyright © 2018 YWQ. All rights reserved.
//

import Foundation
import UIKit

class HYTabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let TabBar = UITabBar.appearance()
        TabBar.tintColor = UIColor.red
        addChildViewControllers()
    }
    
    //添加子控制器
    private func addChildViewControllers(){
        self.setChildViewController(childController: HYNewsViewController(), title: "新闻", imageName: "新闻")
        self.setChildViewController(childController: HYVideosViewController(), title: "视频", imageName: "视频")
        self.setChildViewController(childController: HYRecommendationViewController(), title: "推荐", imageName: "推荐")
        self.setChildViewController(childController: HYMeViewController(), title: "我", imageName: "个人")
    }
    
    //初始化子控制器
    private func setChildViewController(childController:UIViewController, title:String,imageName:String ){
        childController.tabBarItem.image = UIImage(named: imageName + ".png")
        childController.title = title
        // 添加导航控制器为 TabBarController 的子控制器
        let nav = HYNavigationController(rootViewController: childController)
        self.addChildViewController(nav)
    }
}
